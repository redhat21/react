import React, {Component} from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Navbar, NavbarBrand, Breadcrumb, BreadcrumbItem, Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle,  Carousel, CarouselItem, CarouselControl, CarouselIndicators, CarouselCaption, UncontrolledCarousel, Row} from 'reactstrap';

import { BrowserRouter as Router } from "react-router-dom";
import { MDBCol, MDBIcon, MDBFormInline, MDBBtn, MDBNavbarBrand, MDBNavbarToggler, MDBNavbar, MDBCollapse, MDBNavbarNav } from "mdbreact";
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import SkipNextIcon from '@material-ui/icons/SkipNext';

import './App.css';


import BootstrapCarousel from './components/BootstrapCarousel';
import MediaPlayer from './components/MediaPlayer';
import NavbarComp from './components/NavbarComp';


    

class App extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            isLoaded: false
        }
    }

    componentDidMount() {

        fetch('http://localhost:5000/files/')
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    items: json
                })
            });

    }

    state = {
      collapsed: false,
      search: ''
    }

    updateSearch(event) {
      this.setState({search: event.target.value});
    }

    handleTogglerClick = () => {
      this.setState({
        collapsed: !this.state.collapsed,
      });
    }

    handleNavbarClick = () => {
      this.setState({
        collapsed: false
      });
    }    

    render() {

        var { isLoaded, items } = this.state;
        

        if(!isLoaded) {
            return <div>Loading...</div>
        }
        else {

            let filteredItems = items.filter(
                (item) => {
                    return (item.metadata.title.toLowerCase().indexOf(this.state.search) !== -1) || (item.metadata.author.toLowerCase().indexOf(this.state.search) !== -1);
                }
            )
            
            return (
              
                <div>
                    <Navbar dark color="danger" expand="md">
                        <div className="container">
                            <NavbarBrand href="/">Home Library</NavbarBrand>
                        </div>
                        <MDBNavbarToggler onClick={this.handleTogglerClick} />
                        <Router>
                            <MDBCollapse isOpen={this.state.collapsed} navbar>
                                <MDBNavbarNav right onClick={this.handleNavbarClick}>
                                    <MDBFormInline className="md-form">
                                        <MDBIcon icon="search" />
                                        <input  
                                            type="text" 
                                            placeholder="Search" 
                                            aria-label="Search" 
                                            value={this.state.search} 
                                            onChange={this.updateSearch.bind(this)}/>
                                    </MDBFormInline>
                                </MDBNavbarNav>
                            </MDBCollapse>
                        </Router>
                    </Navbar>
    
                    <BootstrapCarousel></BootstrapCarousel>
                    <Navbar dark color="light">
                        <Breadcrumb>
                            <BreadcrumbItem><a href="#">Home Library</a></BreadcrumbItem>
                            <BreadcrumbItem active>Latest</BreadcrumbItem>
                        </Breadcrumb>
                    </Navbar>
                    <Row>
                        {filteredItems.map((item) => {
                            return (
                            
                                <Card width="10%">
                                    
                                    
                                    <CardMedia><img src={item.metadata.image} width="300" height="400" self-align></img></CardMedia>
                                    <CardContent>
                                    <audio controls>
                                        <source src={'http://localhost:5000/files/audio/' + item.filename} type="audio/mpeg" />
                                    </audio>
                                    
                                    
                                        <b>
                                            <CardTitle align="center">
                                                {item.metadata.title}<br/>
                                                {item.metadata.author}<br/>
                                                {item.metadata.published}
                                            </CardTitle>
                                        </b>
                                    
                                    </CardContent>
                                </Card>
                                
                            
                        )
                        }
                        )
                    }
                    </Row>
                    

                </div>
            );
        }

        
    }
}

export default App;
