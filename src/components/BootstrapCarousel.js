import Carousel from 'react-bootstrap/Carousel'
import React, { Component } from 'react'  
 
export class BootstrapCarousel extends Component {  
        render() {  
  
                return (  
                        <div>  
                         <div >  
                          <div className="row title" style={{ marginBottom: "20px" }} >  
                           
                         </div>  
                         </div>  
                         <div className='container-fluid' >  
                         <Carousel>  
                         <Carousel.Item style={{'height':"500px"}} align='center'>  
                         <img style={{'height':"500px"}}  
                         className="d-block w-20"  
                        src={'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1384742501l/10989335.jpg'}  />  
                             
                                 </Carousel.Item  >  
                                 <Carousel.Item style={{'height':"500px"}} align='center'>  
                                 <img style={{'height':"500px"}}  
                                   className="d-block w-20"  
                                    src={'https://kbimages1-a.akamaihd.net/9785e698-1028-4ebf-9d8e-fae51441fe4c/1200/1200/False/around-the-world-in-80-days-8.jpg'}    />  
                                         
                                         </Carousel.Item>  
                                       <Carousel.Item style={{'height':"500px"}} align='center'>  
                                       <img style={{'height':"500px"}}  
                                        className="d-block w-20"  
                                         src={'https://m.media-amazon.com/images/I/51Vm8NauN+L.jpg'}   />  
                                          
                                         </Carousel.Item>  
                                        </Carousel>  
                                </div>  
                        </div>  
                )  
        }  
}  

export default BootstrapCarousel