import React, { Component } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { MDBCol, MDBIcon, MDBFormInline, MDBBtn, MDBNavbarBrand, MDBNavbarToggler, MDBNavbar, MDBCollapse, MDBNavbarNav } from "mdbreact";
import {Navbar, NavbarBrand , Button} from 'reactstrap';

class NavbarComp extends Component {
    state = {
      collapsed: false,
      search: ''
    }

    updateSearch(event) {
      this.setState({search: event.target.value})
    }

    handleTogglerClick = () => {
      this.setState({
        collapsed: !this.state.collapsed,
      });
    }

    handleNavbarClick = () => {
      this.setState({
        collapsed: false
      });
    }

    render() {
      return (
        
        <Navbar dark color="danger" expand="md">
            <div className="container">
              <NavbarBrand href="/">Home Library</NavbarBrand>
            </div>
            <MDBNavbarToggler onClick={this.handleTogglerClick} />
            <Router>
              <MDBCollapse isOpen={this.state.collapsed} navbar>
                <MDBNavbarNav right onClick={this.handleNavbarClick}>
                <MDBFormInline className="md-form">
                  <MDBIcon icon="search" />
                  <input className="form-control form-control-sm ml-3 w-75" 
                      type="text" 
                      placeholder="Search" 
                      aria-label="Search" 
                      value={this.state.search} 
                      onChange={this.updateSearch.bind(this)}/>
                  <Button color="secondary" size="sm" type="submit" className="mr-auto">
                      Go
                    </Button>
                </MDBFormInline>
                </MDBNavbarNav>
              </MDBCollapse>
            </Router>
          </Navbar>
          
          
        );
      }
    }

export default NavbarComp;